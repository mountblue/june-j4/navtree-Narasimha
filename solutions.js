var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017";


function getSeasons(dataBaseName, collectionMatches) {
    return new Promise(function (resolve, reject) {
        var cursor = [];

        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, data) {
            if (err) {
                reject(err);
            } else {
                // console.log("Connection established...");
                const database = data.db(dataBaseName);

                cursor = database.collection(collectionMatches).aggregate([{
                            $group: {
                                _id: "$season"
                            }
                        },
                        {
                            $sort: {
                                _id: 1
                            }
                        }
                    ])
                    .toArray(
                        (function (err, docs) {
                            data.close();
                            // console.log(docs);
                            resolve(docs);
                        }));
            }
        });
    });
}


function getTeamsPerSeason(dataBaseName, collectionMatches, year) {
    return new Promise(function (resolve, reject) {
        var cursor = [];

        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, data) {
            if (err) {
                reject(err);
            } else {
                // console.log("Connection established...");
                const database = data.db(dataBaseName);

                cursor = database.collection(collectionMatches).aggregate([{
                            $match: {
                                season: year
                            }
                        },
                        {
                            $group: {
                                _id: "$winner"
                            }
                        }
                    ])
                    .toArray(
                        (function (err, docs) {
                            data.close();
                            // console.log(docs);
                            resolve(docs);
                        }));
            }
        });
    });
}

function getPlayerNames(dataBaseName, collectionMatches, collectionDeliveries, year, team) {
    return new Promise(function (resolve, reject) {
        var cursor = [];

        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, data) {
            if (err) {
                reject(err);
            } else {
                // console.log("Connection established...");
                const database = data.db(dataBaseName);

                cursor = database.collection(collectionMatches).aggregate([{
                            $match: {
                                season: year
                            }
                        },
                        {
                            $lookup: {
                                from: "deliveries",
                                localField: "id",
                                foreignField: "match_id",
                                as: "new"
                            }
                        },
                        {
                            $unwind: "$new"
                        },
                        {
                            $project: {
                                _id: 0,
                                "team": "$new.batting_team",
                                "player": "$new.batsman"
                            }
                        },
                        {
                            $match: {
                                team: team
                            }
                        },
                        {
                            $group: {
                                _id: "$player"
                            }
                        }
                    ])
                    .toArray(
                        (function (err, docs) {
                            data.close();
                            // console.log(docs);
                            resolve(docs);
                        }));
            }
        });
    });
}

// function getPlayerStats(dataBaseName, collectionMatches, collectionDeliveries, year, team, player) {return new Promise(function (resolve, reject) {
//     var cursor = [];

//     MongoClient.connect(url, {
//         useNewUrlParser: true
//     }, function (err, data) {
//         if (err) {
//             reject(err);
//         } else {
//             // console.log("Connection established...");
//             const database = data.db(dataBaseName);

//             cursor = database.collection(collectionMatches).aggregate([{
//                         $match: {
//                             season: year
//                         }
//                     },
//                     {
//                         $lookup: {
//                             from: "deliveries",
//                             localField: "id",
//                             foreignField: "match_id",
//                             as: "new"
//                         }
//                     },
//                     {
//                         $unwind: "$new"
//                     },
//                     {
//                         $project: {
//                             _id: 0,
//                             "team": "$new.batting_team",
//                             "player": "$new.batsman"
//                         }
//                     },
//                     {
//                         $match: {
//                             team: team
//                         }
//                     },
//                     {
//                         $group: {
//                             _id: "$player"
//                         }
//                     }
//                 ])
//                 .toArray(
//                     (function (err, docs) {
//                         data.close();
//                         // console.log(docs);
//                         resolve(docs);
//                     }));
//         }
//     });
// });
    
// }
// getSeasons("IPL", "matches");
// getTeamsPerSeason("IPL", "matches", 2014);
// getPlayerNames("IPL", "matches", "deliveries",2011, "Mumbai Indians");

module.exports = {
    getSeasons: getSeasons,
    getTeamsPerSeason: getTeamsPerSeason,
    getPlayerNames: getPlayerNames
    // getPlayerStats: getPlayerStats
}