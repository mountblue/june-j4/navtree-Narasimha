var express = require("express");
const path = require("path");
const dataBaseName = "IPL";
const collectionMatches = "matches";
const collectionDeliveries = "deliveries";
const fileName = path.resolve("solutions.js");
const operations = require(fileName);

var app = express();

var PORT = process.env.PORT || 3000;
app.use(express.static(__dirname));
app.get("/", (req, res) => {
    res.sendFile(__dirname);
})

app.get("/api/seasons", async function (req, res){
    const data = await operations.getSeasons(dataBaseName, collectionMatches);
    res.send(data);
});

app.get("/api/seasons/:season", async function (req, res){
    const year = Number(req.params.season);
    // console.log(year);
    const data = await operations.getTeamsPerSeason(dataBaseName, collectionMatches, year);
    res.send(data);
});

app.get("/api/seasons/:seasons/teams/:team", async function(req, res){
    const year = Number(req.params.seasons);
    const team = req.params.team;
    const data = await operations.getPlayerNames(dataBaseName, collectionMatches, collectionDeliveries, year, team);
    res.send(data);
})

// app.get("/seasons/:seasons/:teams/:players", async function(req, res){
//     const year = Number(req.params.seasons);
//     const team = req.params.teams;
//     const player = req.params.players;
//     const data = await operations.getPlayerStats(dataBaseName, collectionMatches, collectionDeliveries, year, team, player);
//     res.send(data);
// })


app.listen(PORT, function () {
    console.log("Server listening on the port " + PORT);
});